
## 服务端运行
1. 安装依赖 根目录下执行该命令
```shell script
npm install 

```
2. 开启服务

根目录下执行 
```shell script
node index.js
// 或者 
npm run serve
```

启动服务



## 客户端
1。 浏览器打开 index.html 即可
