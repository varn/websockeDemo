const WebSocket = require('ws');

const server = new WebSocket.Server({ port: 8093 });

server.on('open', function open() {
    console.log('connected');
});

server.on('close', function close() {
    console.log('disconnected');
});

server.on('connection', function connection(ws, req) {
    // console.log(req);
    // 发送欢迎信息给客户端

    // ws.send("shshshhs")

    ws.on('message', function incoming(message) {
        // console.log( message);
        let data = JSON.parse(message)
        console.log(data);
        let type = data.eventType
        if (type === 'initRoom') {
            let da={
                eventType:'userIn',
                data:{
                    userId: data.data.userId
                }
            }

            ws.send(JSON.stringify(da))
        }
        let index = 1
        setInterval(e=>{
            let data = {
                eventType:'换题',
                value:index
            }
            ws.send(JSON.stringify(data))
            index++
        },1000)

    });

});


function getFormatData(type,data) {
    let d = {
        eventType:type,
        data:data
    }
    return JSON.stringify(d)
}
